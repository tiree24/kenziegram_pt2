const express = require("express");
const path = "./public/uploads";
const multer = require("multer");
const app = express();
// const ejs = require("ejs");
const upload = multer({ dest: "./public/uploads" });
const fs = require("fs");
const port = 3000;

// app.set("view engine", "ejs");

app.set("view engine", "pug");

app.set("views", "./views");

app.use(express.static("./public"));

app.get("/", (req, res) => {
  fs.readdir(path, function (err, items) {
    let imagesTag = items.map((item) => {
      return ` uploads/${item}  `;
    });

    console.log(items);

    res.render("index", { imgsrc: imagesTag });
  });
});

app.post("/upload", upload.single("myFile"), (req, res) => {
  const file = req.file.filename;
  if (!file) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  }
  const src = "uploads/" + file;
  res.render("index2", { imgsrc: src });
});

// app.get("/", (req, res) => res.send("index"));

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));
